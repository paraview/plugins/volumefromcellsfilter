# VolumeFromCellsFilter

## Description
ParaView python plugin adding a filter called 'Volume From Cells' in ParaView.
This filter will compute the volume of each cell of the input `vtkUnstructuredGrid` and generate new cell data ("Volume") containing the result.
This filter can also compute the total volume of the input `vtkUnstructuredGrid` and store the result as field data.

This filter shares similarities wuth the `Cell Size` filter, except that it gives better results with concave cells (`vtkPolyhedron`).
It internally takes advantage of the Green theorem to compute the cell volume from its surfaces.

## How to load the plugin

Like any ParaView python plugin:
- open ParaView
- on the Menu Bar, click on Tools -> Manage Plugins
- there is a dialog box with two sides, left is Remote Plugins,
  and right is Local Plugins
- if you are using the client-server connection, you need to
  load this filter both locally and remotely
  - Local Instructions
    - click on the 'Load New' button, near the middle bottom (local side)
    - choose this file to load
    - you will see a new plugin called 'VolumeFromCells' at the bottom
    - click on the arrow left of 'VolumeFromCells'
    - click the box next to 'Auto Load' to make it automatically
      load in the future
  - Remote Instructions
    - click on the 'Load New' button, near the bottom left (remote side)
    - choose this file to load (will need to copy this file to the
      remote machine)
    - you will see a new plugin called 'VolumeFromCells' at the bottom
    - click on the arrow left of 'VolumeFromCells'
    - click the box next to 'Auto Load' to make it automatically
      load in the future
  - click on the 'Close' button
- if you are only working locally (no client-server connection)
  - load the file only on the local side, using the instructions above

Once the plugin is loaded, the `Volume From Cells` filter can now be found in `Filters -> Alphabetical -> Volume From Cells`.

## Authors and acknowledgment

Copyright (c) Eramet Ideas
All rights reserved.

## License

The VolumeFromCellsFilter plugin is distributed under the BSD-3-CLAUSE license.
You can redistribute it and/or modify it under the terms described in the License.txt file.
