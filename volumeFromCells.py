#
#  Copyright (c) Eramet Ideas
#  All rights reserved.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
#  BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
#  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
#  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#
# Create a filter called 'Volume From Cells'
#
# This filter will, given a vtkUnstructuredGrid, return a new cell data (volume) and a new field data (total volume)
#

# How to load the filter:
#   - open ParaView
#   - on the Menu Bar, click on Tools -> Manage Plugins
#   - there is a dialog box with two sides, left is Remote Plugins,
#     and right is Local Plugins
#   - if you are using the client-server connection, you need to 
#     load this filter both locally and remotely
#     - Local Instructions
#       - click on the 'Load New' button, near the middle bottom (local side)
#       - choose this file to load
#       - you will see a new plugin called 'VolumeFromCells' at the bottom
#       - click on the arrow left of 'VolumeFromCells'
#       - click the box next to 'Auto Load' to make it automatically
#         load in the future
#     - Remote Instructions
#       - click on the 'Load New' button, near the bottom left (remote side)
#       - choose this file to load (will need to copy this file to the
#         remote machine)
#       - you will see a new plugin called 'VolumeFromCells' at the bottom
#       - click on the arrow left of 'VolumeFromCells'
#       - click the box next to 'Auto Load' to make it automatically
#         load in the future
#     - click on the 'Close' button
#   - if you are only working locally (no client-server connection)
#     - load the file only on the local side, using the instructions above
#
#   - there is now an available filter called 'Volume From Cells'.
#   - click on Filters -> Alphabetical -> Volume From Cells

# How to use the filter:
#   - create a 'Volume From Cells' filter on your dataset
#   - in the Properties tab, there is a checkbox called 'Compute Sum'
#     - you can thus decide to calculate or not the vtkUnstructuredGrid total volume
#     - the choice can be changed when necessary
#   - hit the 'Apply' button

import vtk
from vtkmodules.vtkCommonDataModel import vtkDataSet
from vtkmodules.util.vtkAlgorithm import VTKPythonAlgorithmBase
from paraview.util.vtkAlgorithm import smproxy, smproperty, smdomain

# The following lines define the "decorators" of the plugin
# (see https://kitware.github.io/paraview-docs/latest/python/paraview.util.vtkAlgorithm.html)
# 'label' appears in the 'Filters' list window
# 'name' appears in bold in the "Change Input Dialog" box
# 'label' appears in grey when trying to apply to a VTK object different from vtkUnstructuredGrid
# (smdomain.datatype refers to smproperty.input just above >> 'The order of chaining is significant...')

@smproxy.filter(label="Volume From Cells")
@smproperty.input(name="Input")
@smdomain.datatype(dataTypes=["vtkUnstructuredGrid"], composite_data_supported=True)

class GetVolumeFromCells(VTKPythonAlgorithmBase):
    """
    Given a vtkUnstructuredGrid (alone or inside a vtkMultiBlockDataSet), return cell volumes and total volume
    """

    def __init__ (self):
        VTKPythonAlgorithmBase.__init__(self, nInputPorts=1, nOutputPorts=1, outputType="vtkUnstructuredGrid")
        # (checkbox default)
        self._computeSum = True

    def RequestData(self, request, inInfo, outInfo):
        input0 = vtkDataSet.GetData(inInfo[0])
        # set the vtkUnstructuredGrid output
        output = vtk.vtkUnstructuredGrid.GetData(outInfo, 0)
        output.ShallowCopy(input0)
        outputCD = output.GetCellData()
        outputFD = output.GetFieldData()
        cellVolume, totalVolume = self.calculateVolume(input0)
        outputCD.AddArray(cellVolume)
        if (self._computeSum):
            outputFD.AddArray(totalVolume)
        return 1

    def calculateVolume(self, ug):
        """
        Given an unstructured grid, calculate volume of each cell and total volume
        General (and improved) algorithm for polyhedra as it also works for concave cells (based on Green's theorem)
        Also works for hexaedra, tetraedra (and certainly some other vtkCell3D) with correction for InsideOut cells
        """
        from vtk.util import numpy_support
        import numpy

        numberOfCells = ug.GetNumberOfCells()
        TotalVolume = 0.0
        cellVolume = vtk.vtkDoubleArray()
        cellVolume.SetName("Volume")
        Volume = vtk.vtkDoubleArray()
        Volume.SetName("Volume")

        # Calculate volume for each polyhedral cell

        for i in range(0, numberOfCells):
    
            cell = ug.GetCell(i)
            numberOfFaces = cell.GetNumberOfFaces()
            polyhedron_volume = 0.0
    
            # Calculate area and normal for each polygonal face

            for j in range(0, numberOfFaces):
        
                polygon_vertices = numpy_support.vtk_to_numpy(cell.GetFace(j).GetPoints().GetData())
                polygon_centroid = numpy.mean(polygon_vertices, axis=0)
                polygon_area = 0.0
                polygon_normal = numpy.array((0., 0., 0.))
        
                # Triangulate polygon and sum triangle areas
        
                for k in range(1, len(polygon_vertices) - 1):
                    v0 = polygon_vertices[0]
                    v1 = polygon_vertices[k]
                    v2 = polygon_vertices[k+1]
                    cross_product = numpy.cross(v1 - v0, v2 - v0)
                    triangle_area = 0.5 * numpy.linalg.norm(cross_product)
                    polygon_area += triangle_area
                    polygon_normal += cross_product
        
                norm = numpy.linalg.norm(polygon_normal)
                if (norm == 0.0):
                    polygon_normal = numpy.array((0., 0., 0.))
                    print("Warning... degenerated face in cell", i)
                else:
                    polygon_normal = polygon_normal / norm

                dot_product = numpy.dot(polygon_normal, polygon_centroid)
                polyhedron_volume += dot_product * polygon_area

            polyhedron_volume /= 3.0
            if (polyhedron_volume < 0.0):
                polyhedron_volume = -polyhedron_volume
            cellVolume.InsertNextValue(polyhedron_volume)
            TotalVolume += polyhedron_volume

        Volume.InsertNextValue(TotalVolume)

        return cellVolume, Volume

    # This is the checkbox property

    @smproperty.xml("""
        <IntVectorProperty name="Compute Sum"
            number_of_elements="1"
            default_values="1"
            command="SetComputeSum">
          <BooleanDomain name="bool" />
        </IntVectorProperty>""")
    def SetComputeSum(self, checkStatus):
        self._computeSum = checkStatus
        self.Modified()
